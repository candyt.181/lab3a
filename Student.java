public class Student{
	
	public String name;
	public String lastname;
	public int studentID;
	
	public void sayName(){
		if(this.name.equals("Candy")){
			System.out.println("I am Candy, the student #1");
		}
		else {
			System.out.println("I am Rebecca, the student #2");
		}
	}
	
	public void study(){
		if(this.lastname.equals("Tran")){
			System.out.println("Ms.Tran! Study for your test!");
		}
		else {
			System.out.println("Ms.Trudeau! Study for your test!");
		}
	}


}