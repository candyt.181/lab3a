public class Application{
	public static void main(String[] args){
		Student firstStudent = new Student();
		firstStudent.name = "Candy";
		firstStudent.lastname = "Tran";
		firstStudent.studentID = 2142374;
		System.out.println("You are " + firstStudent.name + " " + firstStudent.lastname + " " + firstStudent.studentID);
		
		Student secondStudent = new Student ();
		secondStudent.name = "Rebecca";
		secondStudent.lastname = "Trudeau";
		secondStudent.studentID = 1234567;
		System.out.println("You are " + secondStudent.name + " " + secondStudent.lastname + " " +  secondStudent.studentID);
		
		Student thirdStudent = new Student ();
		thirdStudent.name = "Jane";
		thirdStudent.lastname = "Smith";
		thirdStudent.studentID = 2345678;
		System.out.println("You are " + thirdStudent.name + " " + thirdStudent.lastname + " " +  thirdStudent.studentID);
		
		
		
		firstStudent.sayName();
		secondStudent.sayName();
		firstStudent.study();
		secondStudent.study();
		
		Student[] section4 = new Student[3];
		section4[0] = firstStudent;
		section4[1] = secondStudent;
		section4[2] = new Student();
	
		
		System.out.println(section4[2].name);
		System.out.println(section4[2].lastname);
		System.out.println(section4[2].studentID);
	}
}